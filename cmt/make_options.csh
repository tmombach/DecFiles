# Create options file
./create_options.py
#
# To update (Zoltan's) book-keeping do this by hand or include it in 
# make_a_release
# SetupProject Dirac
# lhcb-proxy-init
# dirac-bookkeeping-eventtype-mgt-insert ../doc/table_event.sql
# dirac-bookkeeping-eventtype-mgt-update ../doc/table_event.sql
