# EventType: 23123221
#
# Descriptor: [D_s- -> K- (eta -> e+ e- gamma)]cc 
#
# NickName: Ds_K+eta,eeg=TightCut
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay     = '^[ D_s+ -> ^K+ ( eta -> ^e+ ^e- gamma )]CC'
# tightCut.Cuts      =    {
#     '[e+]cc'  : ' inAcc & eCuts',
#     '[K+]cc'  : ' inAcc & piCuts',
#     '[D_s+]cc': ' Dcuts ' }
# tightCut.Preambulo += [
#     'inAcc = in_range ( 0.005, GTHETA, 0.400 ) ' , 
#     'eCuts = ( (GPT > 50 * MeV) & ( GP > 600 * MeV))',
#     'piCuts = ( (GPT > 200 * MeV) & ( GP > 600 * MeV))',
#     'Dcuts = (GPT > 1000 * MeV)' ]
# EndInsertPythonCode
#
# Documentation: Ds+ forced to decay to K+ eta, then eta to (e+ e- gamma) with generator level cuts. 
# EndDocumentation
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: < 1 min
# Responsible: Tom Hadavizadeh 
# Email: tom.hadavizadeh@cern.ch
# Date: 20170302
#
Alias      MyEta        eta
ChargeConj MyEta        MyEta
#
Decay D_s-sig
  1.00    K- MyEta           PHSP;
Enddecay
CDecay D_s+sig
#
Decay MyEta
  1.00    e+  e- gamma        PHSP;
Enddecay
#
End
#
