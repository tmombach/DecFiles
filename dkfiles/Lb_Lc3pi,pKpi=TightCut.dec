# EventType: 15266055
#
# Descriptor: [Lambda_b0 -> (Lambda_c+ -> p+ K- pi+) pi- pi+ pi-]cc
#
# NickName: Lb_Lc3pi,pKpi=TightCut 
#
# Cuts: LoKi::GenCutTool/TightCut
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# gen = Generation()
# gen.SignalPlain.addTool ( LoKi__GenCutTool , 'TightCut' )
# # 
# tightCut = gen.SignalPlain.TightCut
# tightCut.Decay = '^[Lambda_b0 ==> (Lambda_c+ ==> ^p+ ^K- ^pi+) ^pi- ^pi+ ^pi-]CC'
# tightCut.Preambulo += [
#    'from GaudiKernel.SystemOfUnits import millimeter,micrometer,MeV,GeV',
#    'inAcc       = in_range ( 0.005 , GTHETA , 0.400 )' ,
#    'inEta       = in_range ( 1.85  , GETA   , 5.050 )' ,
#    'inY         = in_range ( 1.9   , GY     , 4.6   )' ,
#    'goodProton  = ("p+"  == GABSID ) & ( GPT > 0.38 * GeV ) & ( GP  > 8.0 * GeV ) & inAcc & inEta ', 
#    'goodKaon    = ("K+"  == GABSID ) & ( GPT > 0.18 * GeV ) & ( GP  > 2.5 * GeV ) & inAcc & inEta ',
#    'goodPion    = ("pi+" == GABSID ) & ( GPT > 0.18 * GeV ) & ( GP  > 2.5 * GeV ) & inAcc & inEta ',   
#    'goodLambda_b0   =  ( GTIME > 0.05 * millimeter ) &   (GPT > 2.5 * GeV) & inY ',
# ]
# tightCut.Cuts      =    {
#     '[p+]cc'        : 'goodProton'   ,
#     '[K+]cc'        : 'goodKaon'     , 
#     '[pi+]cc'       : 'goodPion'     ,
#     '[Lambda_b0]cc' : 'goodLambda_b0'}
#
# EndInsertPythonCode
#
# Documentation: phase space decay, decay products in acceptance, decay model of S. Blusk (event type 15266005)
# EndDocumentation
#
# PhysicsWG: B2OC
# Tested: Yes
# CPUTime:  6 min
# Responsible: A. Berezhnoy
# Email: alexander.berezhnoy@cern.ch
# Date: 20170912
#
Alias      Mya_1-     a_1-
Alias      Mya_1+     a_1+
ChargeConj Mya_1+     Mya_1-
#
Alias MyLambda_c+ Lambda_c+
Alias Myanti-Lambda_c- anti-Lambda_c-
ChargeConj MyLambda_c+ Myanti-Lambda_c-
#
Alias MyLambda_c(2593)+ Lambda_c(2593)+
Alias Myanti-Lambda_c(2593)- anti-Lambda_c(2593)-
ChargeConj MyLambda_c(2593)+ Myanti-Lambda_c(2593)-
#
Alias MyLambda_c(2625)+ Lambda_c(2625)+
Alias Myanti-Lambda_c(2625)- anti-Lambda_c(2625)-
ChargeConj MyLambda_c(2625)+ Myanti-Lambda_c(2625)-
#
Alias MySigma_c0 Sigma_c0
Alias Myanti-Sigma_c0 anti-Sigma_c0
ChargeConj MySigma_c0 Myanti-Sigma_c0
#
Alias MySigma_c++ Sigma_c++
Alias Myanti-Sigma_c-- anti-Sigma_c--
ChargeConj MySigma_c++ Myanti-Sigma_c--
#
Alias      Myf_2 f_2
ChargeConj Myf_2 Myf_2
#
Alias      MyK*0      K*0
Alias      Myanti-K*0 anti-K*0
ChargeConj MyK*0      Myanti-K*0
#
Alias      Myrho0   rho0
ChargeConj Myrho0   Myrho0
#
Alias      MyDelta++       Delta++
Alias      Myanti-Delta--  anti-Delta--
ChargeConj MyDelta++       Myanti-Delta--
#
Decay Lambda_b0sig
  0.53    MyLambda_c+        Mya_1-         PHSP;
  0.10    MyLambda_c+        Myrho0  pi-    PHSP;
  0.14    MyLambda_c+        Myf_2   pi-    PHSP;
  0.03    MyLambda_c(2593)+  pi-            PHSP;
  0.06    MyLambda_c(2625)+  pi-            PHSP;
  0.05    MySigma_c++        pi-  pi-       PHSP;
  0.09    MySigma_c0         pi+  pi-       PHSP;
Enddecay
CDecay anti-Lambda_b0sig
#
Decay Mya_1+
  1.000   Myrho0 pi+       VVS_PWAVE 1.0 0.0 0.0 0.0 0.0 0.0;
Enddecay
CDecay Mya_1-
#
Decay MyLambda_c+
  0.008600000 MyDelta++ K-                                   PHSP;
  0.010700000 p+        Myanti-K*0                           PHSP;
  0.025400000 p+        K-      pi+                          PHSP;
Enddecay
CDecay Myanti-Lambda_c-
#
Decay MyLambda_c(2593)+
  0.24000      MySigma_c++         pi-                      PHSP; 
  0.24000      MySigma_c0          pi+                      PHSP;
  0.18000      MyLambda_c+         pi+    pi-               PHSP;
Enddecay
CDecay Myanti-Lambda_c(2593)-
#
Decay MyLambda_c(2625)+
  1.0000     MyLambda_c+  pi+  pi-            PHSP;
Enddecay
CDecay Myanti-Lambda_c(2625)-
#
Decay MySigma_c++
  1.0000    MyLambda_c+  pi+                  PHSP;
Enddecay
CDecay Myanti-Sigma_c--
#
Decay MySigma_c0
  1.0000    MyLambda_c+  pi-                  PHSP;
Enddecay
CDecay Myanti-Sigma_c0
#
Decay Myf_2
  1.0000  pi+ pi-                             TSS;
Enddecay
#
Decay MyK*0
  1.000   K+  pi-                             VSS;
Enddecay
CDecay Myanti-K*0
#
Decay Myrho0
  1.0000  pi+ pi-                             VSS;
Enddecay
#
Decay MyDelta++
  1.0000  p+  pi+                             PHSP;
Enddecay
CDecay Myanti-Delta--

End
