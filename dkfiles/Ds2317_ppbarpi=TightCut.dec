# EventType: 27103091
#
# Descriptor: [D*_s0+ -> p+ p~- pi+]cc
#
# NickName: Ds2317_ppbarpi=TightCut
#
# ParticleValue: "D*_s0+                171       10431   1.0      2.31770000      6.582100e-22                    D_s0*+       10431      0.005", "D*_s0-                175      -10431  -1.0      2.31770000      6.582100e-22                    D_s0*-      -10431      0.005"
#
# Cuts: LoKi::GenCutTool/TightCut
#
# Documentation:
#    Weak decays of Ds2317 to ppbarpi final state.
#    Daughters in LHCb.
# EndDocumentation
#
# InsertPythonCode:
# #
# from Configurables import LoKi__GenCutTool
# from Gauss.Configuration import *
# generation = Generation() 
# signal     = generation.SignalPlain 
# signal.addTool ( LoKi__GenCutTool , 'TightCut' )
# tightCut   = signal.TightCut
# tightCut.Decay     = '^[D*_s0+ => ^p+ ^p~- ^pi+]CC'
# tightCut.Preambulo += [
#     'from GaudiKernel.SystemOfUnits import millimeter, micrometer,MeV,GeV',
#     'inAcc        =  in_range ( 0.005 , GTHETA , 0.400 )               ' ,
#     'fastTrack    =  ( GPT > 200 * MeV ) & ( GP  > 1.9 * GeV )         ' , 
#     'goodTrack    =  inAcc &  fastTrack                                ' ,     
#     'goodDs       =  ( GPT > 0.9 * GeV )         ' ,
#     'Bancestors   =  GNINTREE ( GBEAUTY , HepMC.ancestors )            ' , 
#     'notFromB     =  0 == Bancestors                                   ' 
# ]
#
# tightCut.Cuts     =    {
#     '[D*_s0+]cc': 'goodDs & notFromB ' ,
#     '[p+]cc'  : 'goodTrack ' , 
#     '[pi+]cc' : 'goodTrack '
#     }
# EndInsertPythonCode
#
# PhysicsWG: Charm
# Tested: Yes
# CPUTime: 10 min 
# Responsible: Marco Pappagallo
# Email: marco.pappagallo@cern.ch
# Date: 20170205
#
Decay D_s0*+sig
  1.000        p+      anti-p-       pi+             PHSP;
Enddecay
CDecay D_s0*-sig
#
End
