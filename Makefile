export DECFILESROOT=$(shell pwd)

all: doc/newdecfiles.txt .create_options.stamp

.create_options.stamp: .FORCE
	md5sum dkfiles/*.dec > $@.tmp
	test -e $@ || touch $@
	diff -q $@ $@.tmp || ($(DECFILESROOT)/cmt/create_options.py && mv -f $@.tmp $@)

doc/newdecfiles.txt: .create_options.stamp
	$(DECFILESROOT)/cmt/listNewEvtTypes.py

test: all
	@for evt_type in $$(python -c "print '\n'.join(eval(open('doc/newdecfiles.txt').read())['new_options'])") ; do \
		echo testing $$evt_type ; \
		./scripts/testDecayFile.py $$evt_type || exit $$? ; \
	done

clean: 
	rm .create_options.stamp options/[1-9]*py

.FORCE:
